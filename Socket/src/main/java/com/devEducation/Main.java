package com.devEducation;

import com.devEducation.socket.ServerSocket;

public class Main {
    public static void main(String[] args) throws Exception {
    final int PORT = 9001;

    ServerSocket serverSocket = new ServerSocket(PORT);
    serverSocket.client();
    }

}
