package com.devEducation.socket;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;

public class ServerSocket {

    private ServerSocketChannel serverSocketChannel;
    private ByteBuffer byteBuffer = ByteBuffer.allocate(1024);
    SocketChannel socketChannelClient = null;

    public ServerSocket(int port) {
        try {
            serverSocketChannel = ServerSocketChannel.open();
            serverSocketChannel.socket().bind(new InetSocketAddress(port));
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public SocketChannel initialize(int port) throws IOException {
        serverSocketChannel = ServerSocketChannel.open();
        serverSocketChannel.socket().bind(new InetSocketAddress(port));
        return serverSocketChannel.accept();
    }

    public ByteBuffer getByteBuffer() {
        return byteBuffer;
    }

    public SocketChannel accept() throws IOException {
        return serverSocketChannel.accept();
    }

    public void client() throws IOException {
        while (true) {
            try {
                socketChannelClient = accept();
            } catch (IOException e) {
                e.printStackTrace();
            }
            ByteBuffer byteBuffer = getByteBuffer();
            String response = "";
            if (socketChannelClient.read(byteBuffer) > 0) {
                response = new String(byteBuffer.array(), "UTF-8");
                byteBuffer.clear();
            }
        }
    }
}
