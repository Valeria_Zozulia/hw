package com.devEducation.model;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.Random;

public class Generator {
    public void generator(ByteBuffer byteBuffer, SocketChannel socketChannelClient) throws IOException, InterruptedException {
        Random random = new Random();

        for (int i = 0; i < 100; i++) {
            int temp = random.nextInt(1000);
            System.out.println(temp);
            //byteBuffer.flip();

            while (byteBuffer.hasRemaining()) {
                socketChannelClient.write(byteBuffer);
            }
            Thread.sleep(1000);
        }
    }


}
