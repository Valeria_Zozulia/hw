package sample;

import java.sql.*;

public class People {
    private static Statement statement;
    private static ResultSet resultSet;
    private static PreparedStatement preparedStatement;
    private static Connection connection = null;

    //Вывести общее число жителей
    public static int amountOfPeople() {
        String query = "SELECT COUNT(*) FROM Person";
        int count = 0;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                count = resultSet.getInt(1);
            }
            statement = connection.prepareStatement(query);
            statement.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
        }return count;
    }
}
