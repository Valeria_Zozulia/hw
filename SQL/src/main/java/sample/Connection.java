package sample;

import java.sql.*;

public class Connection {
    static final String url = "jdbc:mysql://eu-cdbr-west-03.cleardb.net:3306/";

    private static java.sql.Connection connection;
    private static Statement statement;
    private static PreparedStatement preparedStatement;
    private static ResultSet resultSet;

    public static void mySQLConnect() {

        try {
            connection = DriverManager.getConnection("jdbc:mysql://eu-cdbr-west-03.cleardb.net:3306/");
            statement = connection.createStatement();

        } catch (SQLException sqlEx) {
            System.out.println("Connection failed.");
            sqlEx.printStackTrace();
        }

        if (connection != null) {
            System.out.println("Connect to data base success.");
        } else {
            System.out.println("Failed to connect.");
        }
    }

    public Statement createStatement() {
        return null;
    }

    public String prepareStatement(String query) {
        return "";
    }
}
