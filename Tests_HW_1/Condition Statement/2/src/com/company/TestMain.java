package com.company;

import org.junit.Assert;
import org.junit.Test;

public class TestMain {
    @Test
    public void FailedFineCoordinatePlane(){
        String act = Main.FineCoordinatePlane(0, 0);
        Assert.assertEquals("Unable to determine the coordinate plane", act);
    }
    @Test
    public void FirstCoordinatePlaneTest(){
        String act = Main.FineCoordinatePlane(2, 5);
        Assert.assertEquals("The point belongs to the first coordinate plane", act);
    }
    @Test
    public void SecondCoordinatePlaneTest(){
        String act = Main.FineCoordinatePlane(-6, 3);
        Assert.assertEquals("The point belongs to the second coordinate plane",act);
    }
    @Test
    public void ThirdCoordinatePlaneTest(){
        String act = Main.FineCoordinatePlane(-6, -8);
        Assert.assertEquals("The point belongs to the third coordinate plane",act);
    }
    @Test
    public void FourthCoordinatePlaneTest(){
        String act = Main.FineCoordinatePlane(9, -2);
        Assert.assertEquals("The point belongs to the fourth coordinate plane",act);
    }
}

