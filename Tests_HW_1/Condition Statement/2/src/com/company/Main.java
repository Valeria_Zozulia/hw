package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Write coordinate х:");
        int x = in.nextInt();
        System.out.println("Write coordinate у:");
        int y = in.nextInt();
        System.out.println(FineCoordinatePlane(x, y));
    }

    public static String FineCoordinatePlane(int x, int y) {
        if (x == 0 || y == 0) {
            return "Unable to determine the coordinate plane";
        } else if (x > 0 && y > 0) {
            return "The point belongs to the first coordinate plane";
        } else if (x < 0 && y > 0) {
            return "The point belongs to the second coordinate plane";
        } else if (x < 0 && y < 0) {
            return "The point belongs to the third coordinate plane";
        } else if (x > 0 && y < 0) {
            return "The point belongs to the fourth coordinate plane";
        } else {
            return "0";
        }
    }
}