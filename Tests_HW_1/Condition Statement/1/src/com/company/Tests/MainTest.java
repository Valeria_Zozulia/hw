package com.company.Tests;

import com.company.Main;
import org.junit.Assert;
import org.junit.Test;

public class MainTest {
    @Test
    public void SucessfullSomeFunctionTest(){
        int act = Main.SomeFunction(2,2);
        Assert.assertEquals(4, act);
    }

    @Test
    public void FailedSomeFunctionTest(){
        int act = Main.SomeFunction(-10,2);
        Assert.assertEquals(-20, act);
    }
}
