package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        while(true){
            Scanner in = new Scanner(System.in);
            System.out.println("Write first number: ");
            int a = in.nextInt();
            System.out.println("Write second number: ");
            int b = in.nextInt();
            System.out.println(SomeFunction(a,b));
        }
    }

    public static int SomeFunction(int a, int b){
        if(a % 2 == 0) return a * b;
        else return a + b;
    }
}