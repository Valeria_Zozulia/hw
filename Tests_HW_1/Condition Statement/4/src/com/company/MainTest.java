package com.company;

import org.junit.Assert;
import org.junit.Test;

public class MainTest {
    @Test
    public void FirstTestFineMaxValue() {
        int act = Main.FineMaxValue(2, 3, 4);
        Assert.assertEquals(27, act);
    }

    @Test
    public void SecondTestFineMaxValue() {
        int act = Main.FineMaxValue(1, 1, 1);
        Assert.assertEquals(6, act);
    }

    @Test
    public void FirstFailedFineMaxValue() {
        int act = Main.FineMaxValue(0, 0, 0);
        Assert.assertEquals(0, act);
    }
}