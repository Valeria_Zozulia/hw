package com.company;

import org.junit.Assert;
import org.junit.Test;

public class MainTest {
    @Test
    public void FirstTestFineEvaluationMark(){
        String act = Main.FineEvaluationMark(18);
        Assert.assertEquals("Your mark: F", act);
    }
    @Test
    public void SecondTestFineEvaluationMark(){
        String act = Main.FineEvaluationMark(25);
        Assert.assertEquals("Your mark: E", act);
    }
    @Test
    public void ThirdTestFineEvaluationMark(){
        String act = Main.FineEvaluationMark(44);
        Assert.assertEquals("Your mark: D", act);
    }
    @Test
    public void FourthTestFineEvaluationMark(){
        String act = Main.FineEvaluationMark(74);
        Assert.assertEquals("Your mark: C", act);
    }
    @Test
    public void FifthTestFineEvaluationMark(){
        String act = Main.FineEvaluationMark(88);
        Assert.assertEquals("Your mark: B", act);
    }
    @Test
    public void SixthTestFineEvaluationMark(){
        String act = Main.FineEvaluationMark(97);
        Assert.assertEquals("Your mark: A", act);
    }
    @Test
    public void FirstFailedFineEvaluationMark(){
        String act = Main.FineEvaluationMark(-16);
        Assert.assertEquals("This does not happen", act);
    }
    @Test
    public void SecondFailedFineEvaluationMark(){
        String act = Main.FineEvaluationMark(144);
        Assert.assertEquals("You are good student", act);
    }
}
