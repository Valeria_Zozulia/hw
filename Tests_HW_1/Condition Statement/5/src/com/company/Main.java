package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Write the number of points: ");
        int a = in.nextInt();
        System.out.println(FineEvaluationMark(a));
    }
    public static String FineEvaluationMark(int a) {
        if (a > 0 && a <= 19) {
            return "Your mark: F";
        } else if (a >= 20 && a <= 39) {
            return "Your mark: E";
        } else if (a >= 40 && a <= 59) {
            return "Your mark: D";
        } else if (a >= 60 && a <= 74) {
            return "Your mark: C";
        } else if (a >= 75 && a <= 89) {
            return "Your mark: B";
        } else if (a >= 90 && a <= 100) {
            return "Your mark: A";
        } else if (a < 0) {
            return "This does not happen";
        } else if (a > 100){
            return "You are good student";
        } else{
            return "0";
        }
    }
}
