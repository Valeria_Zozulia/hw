package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Write number а: ");
        int a = in.nextInt();
        System.out.print("Write number b: ");
        int b = in.nextInt();
        System.out.print("Write number c: ");
        int c = in.nextInt();
        System.out.print(FineSum(a, b, c));
    }

    public static int FineSum(int a, int b, int c) {
        if (a < 0 && b < 0 && c < 0
                || a < 0 && b < 0 && c > 0
                || a > 0 && b < 0 && c < 0
                || a < 0 && b > 0 && c < 0) {
            return 0;
        } else if (a >= 0 && b >= 0 && c >= 0) {
            return a + b + c;
        } else if (a >= 0 && b >= 0 && c <= 0) {
            return a + b;
        } else if (a >= 0 && b <= 0 && c >= 0) {
            return a + c;
        } else if (a <= 0 && b >= 0 && c >= 0) {
            return b + c;
        } else {
            return 0;
        }
    }
}