package com.company;

import org.junit.Assert;
import org.junit.Test;

public class TestMain {

    @Test
    public void FirstFailedFineSum(){
        int act = Main.FineSum(-1, -2, -3);
        Assert.assertEquals(0, act);
    }

    @Test
    public void SecondFailedFineSum(){
        int act = Main.FineSum(-6, -3, 1);
        Assert.assertEquals(0, act);
    }

    @Test
    public void ThirdFailedFineSum(){
        int act = Main.FineSum(1, -4, -9);
        Assert.assertEquals(0, act);
    }

    @Test
    public void FourthFailedFineSum(){
        int act = Main.FineSum(-5, 2, -7);
        Assert.assertEquals(0, act);
    }

    @Test
    public void FirstTestFineSum(){
        int act = Main.FineSum(1, 2, 3);
        Assert.assertEquals(6, act);
    }

    @Test
    public void SecondTestFineSum(){
        int act = Main.FineSum(7, 4, -3);
        Assert.assertEquals(11, act);
    }

    @Test
    public void ThirdTestFineSum(){
        int act = Main.FineSum(8, -2, 6);
        Assert.assertEquals(14, act);
    }

    @Test
    public void FourthTestFineSum(){
        int act = Main.FineSum(-1, 10, 7);
        Assert.assertEquals(17, act);
    }
}
