package com.company;

public class Main {

    public static void main(String[] args) {
        int[] num = new int[]{3, 4, 1, 5, 2};
        int max = num[0];
        int indexMax = 0;
        for (int i = 0; i < num.length; i++) {
            if (num[i] > max) {
                max = num[i];
                indexMax = i;
            }
        }
        System.out.println("indexMax: " + indexMax);
    }
}