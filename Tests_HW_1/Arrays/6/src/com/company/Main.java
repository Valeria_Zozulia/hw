package com.company;

public class Main {

    public static void main(String[] args) {
        int[] a = new int[]{1, 2, 3, 4, 5};
        int b = a.length;
        int temp = 0;
        for(int i = 0; i < b / 2; i++){
            temp = a[b - i - 1];
            a[b - i - 1] = a[i];
            a[i] = temp;
        }
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i]);
        }
    }
}
