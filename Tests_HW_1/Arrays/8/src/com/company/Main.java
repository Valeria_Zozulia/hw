package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Write array length: ");
        int[] num = new int[in.nextInt()];
        for (int i = 0; i < num.length; i++) {
            num[i] = (int) (1 + Math.random() * 30);
            System.out.print("[" + num[i] + "]");
        }
        int mid = num.length / 2;
        int temp1 = num[mid];
        int temp2 = 0;
        for(int i = 0; i < num.length / 2; i++){
            if(num.length % 2 == 0){
            temp2 = num[i];
            num[i] = num[mid];
            num[mid] = temp2;
                mid++;
            } else {
                temp2 = num[i];
                num[i] = temp1;
                temp1 = num[mid+1];
                num[mid+1] = temp2;
                mid++;
                if(i == num.length / 2 - 1){
                    num[mid - i - 1] = temp1;
                }
            }
        }
        System.out.print("\n" + "Array: ");
        for (int i = 0; i < num.length; i++) {
            System.out.print("[" + num[i] + "]");
        }
    }
}
