package com.company;

public class Main {

    public static void main(String[] args) {
        int[] num = new int[]{3, 4, 1, 5, 2};
        int min = num[0];
        int indexMin = 0;
        for (int i = 0; i < num.length; i++) {
            if (num[i] < min) {
                min = num[i];
                indexMin = i;
            }
        }
        System.out.println("indexMin: " + indexMin);
    }
}