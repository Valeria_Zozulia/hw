package com.company;

public class Main {

    public static void main(String[] args) {
        int[] num = new int[]{1, 2, 3, 4, 5};
        int min = num[0];
        for (int i = 0; i < num.length; i++) {
            if (min > num[i]) {
                min = num[i];
            }
        }
        System.out.println("min: " + min);
    }
}
