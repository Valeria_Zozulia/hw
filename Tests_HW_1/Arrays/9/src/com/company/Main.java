package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("1.Bubble" +
                "\n" + "2.Select" +
                "\n" + "3.Insert" + "\n" + "Choose sort: ");
        int a = in.nextInt();
        if (a > 3 || a < 0) {
            System.out.print("Вы ошиблись с вводом");
        } else {
            switch (a) {
                case 1:
                    BubbleSort(in);
                    break;
                case 2:
                    SelectSort(in);
                    break;
                case 3:
                    InsertSort(in);
                    break;
            }
        }
    }

    public static int[] BubbleSort(Scanner in) {
        int[] num = UserInput(in);
        Print(num, "start");
        boolean isSorted = false;
        int temp;
        while (!isSorted) {
            isSorted = true;
            for (int i = 0; i < num.length - 1; i++) {
                if (num[i] > num[i + 1]) {
                    isSorted = false;
                    temp = num[i];
                    num[i] = num[i + 1];
                    num[i + 1] = temp;
                }
            }
        }
        Print(num, "end");
        return num;
    }

    public static int[] SelectSort(Scanner in) {
        int[] num = UserInput(in);
        Print(num, "start");
        int key = 0;
        for (int i = 0; i < num.length; i++) {
            for (int j = i + 1; j < num.length; j++) {
                if (num[i] > num[j]) {
                    key = num[j];
                    num[j] = num[i];
                    num[i] = key;
                }
            }
        }
        Print(num, "end");
        return num;
    }

    public static int[] InsertSort(Scanner in) {
        int[] num = UserInput(in);
        Print(num, "start");
        int a = 0;
        int j = 0;
        for (int i = 0; i < num.length; i++) {
            a = num[i];
            for (j = i - 1; j >= 0 && num[j] > a; j--) {
                    num[j + 1] = num[j];
            }
            num[j + 1] = a;
        }
        Print(num, "end");
        return num;
    }

    public static int[] UserInput(Scanner in) {
        System.out.print("Write array length: ");
        int[] num = new int[in.nextInt()];
        return num;
    }

    public static int[] Print(int[] num, String select) {
        if (select == "start") {
            for (int i = 0; i < num.length; i++) {
                num[i] = (int) (1 + Math.random() * 50);
                System.out.print("[" + num[i] + "]");
            }
        } else if (select == "end") {
            System.out.print("\n" + "Array: ");
            for (int item : num) {
                System.out.print("[" + item + "]");
            }
        }
        return num;
    }
}
