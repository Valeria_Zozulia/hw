package com.company;

import java.io.*;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Write your number: ");
        int a = in.nextInt();
        Function(a);
    }

    public static void Function(int a) {
        if (a == 0) {
            System.out.print("zero");
        }
        int c = a;
        String[] num1 = {"zero","one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};
        String[] num2 = {"and","ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
        String[] num3 = {"", "one hundred", "two hundred", "three hundred", "four hundred", "five hundred",
                "six hundred", "seven hundred", "eight hundred", "nine hundred"};
        String[][] array2 = {num1, num2, num3};
        String[] array = new String[String.valueOf(a).length()];
        for (int i = 0; i < String.valueOf(a).length(); i++) {
            int b = String.valueOf(c).length();
            array[b - 1] = array2[i][(c % 10)];
            c = c / 10;
        }
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }
}
