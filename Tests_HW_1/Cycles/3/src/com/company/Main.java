package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите число: ");
        int a = in.nextInt();
        if (a <= 0) {
            System.out.println("Невозможно определить корень");
        }
        for (int b = 0; b == a / 2; b++) {
            if (b * b == a) {
                System.out.println(b);
                return;
            }
            if (b * b > a) {
                System.out.println(a - 1);
                return;
            }
        }
    }
}
