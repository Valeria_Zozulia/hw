package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class Controller {

    private int operand1;
    private int operand2;

    @FXML
    private Button button;

    @FXML
    private TextField textField1;

    @FXML
    private TextField textField2;

    @FXML
    private TextField resultField;

    @FXML
    private TextField operation;

    @FXML
    public void click (ActionEvent event){
        try{
            operand1 = Integer.valueOf(textField1.getText());
            operand2 = Integer.valueOf(textField2.getText());

            resultField.setText(calculate(operand1, operand2, operation.getText()));
        }catch (Exception e){
            resultField.setText("Error");
        }

    }

    public String calculate(int operand1, int operand2, String operation){
        if(operation == null || operation.isEmpty()) throw new NullPointerException();
        switch (operation){
            case "+":
                return String.valueOf(operand1 + operand2);
            case "-":
                return String.valueOf(operand1 - operand2);
            case "*":
                return String.valueOf(operand1 * operand2);
            case "/":
                return String.valueOf(operand1 / operand2);
            default: return "Error";
        }
    }
}
