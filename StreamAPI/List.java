package com.company;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class List {
    static java.util.List<Student> studentsOfGivenFaculty(ArrayList<Student> arrayList, String faculty) {
        arrayList.stream()
                .filter((student) -> student.getFaculty().equals(faculty))
                .collect(Collectors.toList());

        return arrayList;
    }

    static java.util.List<Student> studentsForEachFacultyAndCourse(ArrayList<Student> arrayList, String faculty, String course) {
        arrayList.stream()
                .filter((student) -> student.getFaculty()
                        .equals(faculty) && student.getCourse() == course)
                .collect(Collectors.toList());

        return arrayList;
    }

    static java.util.List<Student> studentsInYearOfBirth(ArrayList<Student> arrayList, int yearOfBirth) {
        arrayList.stream()
                .filter((student) -> student.getYearOfBirth() >= yearOfBirth)
                .collect(Collectors.toList());

        return arrayList;
    }

    static java.util.List<String> studentsOfStudyGroup(ArrayList<Student> arrayList, String group) {

        java.util.List<String> list = arrayList.stream()
                .filter((student) -> student.getGroup().equals(group))
                .map(student -> student.getFirstName()
                        + student.getLastName()
                        + student.getPatronymic())
                .collect(Collectors.toList());

        return list;
    }

    static java.util.List<String> studentsOfGroup(ArrayList<Student> arrayList) {

        java.util.List<String> list = arrayList.stream()
                .map(student -> student.getFirstName()
                        + student.getLastName()
                        + student.getPatronymic()
                        + student.getFaculty()
                        + student.getGroup())
                .collect(Collectors.toList());

        for (String str : list) {
            System.out.println(str);
        }

        return list;
    }

    static long countStudent(ArrayList<Student> arrayList, String course) {
        return arrayList.stream()
                .filter(student -> course == student.getCourse())
                .count();
    }
}
