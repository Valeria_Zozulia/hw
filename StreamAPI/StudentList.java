package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class StudentList {

    public static List<Student> studentsOfGivenFaculty(ArrayList<Student> studentArrayList, String faculty) {
        return studentArrayList.stream()
                .filter((student) -> student.getFaculty().equals(faculty))
                .collect(Collectors.toList());
    }

    public static List<Student> studentsForEachFacultyAndCourse(ArrayList<Student> studentArrayList, String faculty, String course) {
        return studentArrayList.stream()
                .filter((student) -> student.getFaculty().equals(faculty) && student.getCourse() == course)
                .collect(Collectors.toList());
    }

    public static List<Student> studentsInYearOfBirth(ArrayList<Student> studentArrayList, int year) {
        return studentArrayList.stream()
                .filter((student) -> student.getYearOfBirth() >= year)
                .collect(Collectors.toList());
    }

    public static List<String> studentsOfStudyGroup(ArrayList<Student> studentArrayList, String group) {
        return studentArrayList.stream()
                .filter((student) -> student.getGroup().equals(group))
                .map(student -> student.getFirstName() + student.getLastName() + student.getPatronymic())
                .collect(Collectors.toList());
    }

    public static List<String> studentsOfGroup(ArrayList<Student> studentArrayList) {
        return studentArrayList.stream()
                .map(student -> student.getFirstName()
                        + student.getLastName()
                        + student.getPatronymic()
                        + student.getFaculty()
                        + student.getGroup())
                .collect(Collectors.toList());
    }

    public static long countStudent(ArrayList<Student> studentArrayList, String group) {
        return (int) studentArrayList.stream()
                .filter((student) -> student.getGroup().equals(group))
                .count();
    }
}
