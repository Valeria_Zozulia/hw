package com.company;

import java.util.ArrayList;
import java.util.Scanner;


public class Main {
    public static void main(String[] args) {
        ArrayList<Student> arrayList = new ArrayList<>();
        arrayList.add(new Student(1, "Valeria", "Zozulia", "Andreevna", 1997,
                "Kyiv", "0669605551", "International Economics", "5", "IE-15"));

        arrayList.add(new Student(2, "Anastasia", "Chepurnova", "Vitalievna", 1998,
                "Krivoy Rog", "0962568431", "Economics", "4", "EC-15"));

        arrayList.add(new Student(3, "Platon", "Voloshin", "Nikolaevich", 1998,
                "Krivoy Rog", "0685432112", "Economics", "4", "EC-15"));

        arrayList.add(new Student(4, "Katerina", "Sherstyk", "Ivanovna", 1997,
                "Kyiv", "0967783652", "International Economics", "5", "IE-15"));

        arrayList.add(new Student(5, "Anna", "Chebotar", "Nikolaevna", 1999,
                "Lviv", "0975362519", "Management", "2", "Man-17"));

        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the name of the faculty: ");
        String studentsFaculty = scanner.nextLine();
        System.out.println(StudentList.studentsOfGivenFaculty(arrayList, studentsFaculty));

        System.out.println("Enter the year and a list of students will be displayed: ");
        int year = Integer.parseInt(scanner.nextLine());
        System.out.println(StudentList.studentsInYearOfBirth(arrayList, year));

        System.out.println("Enter the name of the study group: ");
        String groupName = scanner.nextLine();
        System.out.println(StudentList.studentsOfStudyGroup(arrayList, groupName));

        System.out.println("A list of students will be displayed: ");
        System.out.println(StudentList.studentsOfGroup(arrayList));

        System.out.println("Enter the name of the group in which you want to know the number of students: ");
        String countStudent = scanner.nextLine();
        System.out.println(StudentList.countStudent(arrayList, countStudent));
    }
}
