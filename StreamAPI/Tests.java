package com.company;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

public class Tests {
    ArrayList<Student> test = new ArrayList<>();

    @BeforeEach
    public void createTest() {
        test.add(new Student(1, "Valeria", "Zozulia", "Andreevna", 1997,
                "Kyiv", "0669605551", "International Economics", "5", "IE-15"));

        test.add(new Student(2, "Anastasia", "Chepurnova", "Vitalievna", 1998,
                "Krivoy Rog", "0962568431", "Economics", "4", "EC-15"));

        test.add(new Student(3, "Platon", "Voloshin", "Nikolaevich", 1998,
                "Krivoy Rog", "0685432112", "Economics", "4", "EC-15"));

        test.add(new Student(4, "Katerina", "Sherstyk", "Ivanovna", 1997,
                "Kyiv", "0967783652", "International Economics", "5", "IE-15"));

        test.add(new Student(5, "Anna", "Chebotar", "Nikolaevna", 1999,
                "Lviv", "0975362519", "Management", "2", "Man-17"));

        test.add(new Student(6, "Bohdan", "Evtushenko", "Andreevich", 1996,
                "Kyiv", "0976543211", "Management", "2", "Man-17"));
    }

    @Test
    public void studentsOfGivenFaculty() {

        //given
        String faculty = "Management";

        //when
        String expected = "[\n" +
                "id: 6, \n" +
                "firstName: Bohdan, \n" +
                "lastName: Evtushenko, \n" +
                "patronymic: Andreevich, \n" +
                "yearOfBirth: 1996, \n" +
                "address: Kyiv, \n" +
                "telephone: 0976543211, \n" +
                "faculty: Management, \n" +
                "course: 2, \n" +
                "group: Man-17; \n" +
                "id: 5, \n" +
                "firstName: Anna, \n" +
                "lastName: Chebotar, \n" +
                "patronymic: Vitalievna, \n" +
                "yearOfBirth: 1998, \n" +
                "address: Krivoy Rog, \n" +
                "telephone: 0975362519, \n" +
                "faculty: Management, \n" +
                "course: 2, \n" +
                "group: Man-17; \n" +
                "]";
        List<Student> actual = StudentList.studentsOfGivenFaculty(test, faculty);

        //then
        assertEquals(expected, String.valueOf(actual));
        assertNotEquals(0, String.valueOf(actual));
    }

    @Test
    public void shouldGetStudentsOnFacultyAndCourse() {

        //given
        String faculty = "Economics";
        String course = "4";

        //when
        String expected = "[\n" +
                "id: 2, \n" +
                "firstName: Anastasia, \n" +
                "lastName: Chepurnova, \n" +
                "patronymic: Vitalievna, \n" +
                "yearOfBirth: 1998, \n" +
                "address: Krivoy Rog, \n" +
                "telephone: 0962568431, \n" +
                "faculty: Economics, \n" +
                "course: 4, \n" +
                "group: EC-15; \n" +
                "id: 3, \n" +
                "firstName: Platon, \n" +
                "lastName: Voloshin, \n" +
                "patronymic: Nikolaevich, \n" +
                "yearOfBirth: 1998, \n" +
                "address: Krivoy Rog, \n" +
                "telephone: 0685432112, \n" +
                "faculty: Economics, \n" +
                "course: 4, \n" +
                "group: EC-15; \n" +
                "]";
        List<Student> actual = StudentList.studentsForEachFacultyAndCourse(test, faculty, course);

        //then
        assertEquals(expected, String.valueOf(actual));
        assertNotEquals(0, String.valueOf(actual));
    }

    @Test
    public void studentsInYearOfBirth() {

        //given
        int yearOfBirth = 1997;

        //when
        String expected = "[\n" +
                "id: 1, \n" +
                "firstName: Valeria, \n" +
                "lastName: Zozulia, \n" +
                "patronymic: Andreevna, \n" +
                "yearOfBirth: 1997, \n" +
                "address: Kyiv, \n" +
                "telephone: 0669605551, \n" +
                "faculty: International Economics, \n" +
                "course: 5, \n" +
                "group: IE-15; \n" +
                "id: 4, \n" +
                "firstName: Katerina, \n" +
                "lastName: Sherstyk, \n" +
                "patronymic: Ivanovna, \n" +
                "yearOfBirth: 1997, \n" +
                "address: Kyiv, \n" +
                "telephone: 0967783652, \n" +
                "faculty: International Economics, \n" +
                "course: 5, \n" +
                "group: IE-15; \n" +
                "]";
        List<Student> actual = StudentList.studentsInYearOfBirth(test, yearOfBirth);

        //then
        assertEquals(expected, String.valueOf(actual));
        assertNotEquals(0, String.valueOf(actual));
    }

    @Test
    public void studentsOfStudyGroup() {

        //given
        String group = "Man-17";

        //when
        String expected = "[]";
        List<String> actual = StudentList.studentsOfStudyGroup(test, group);

        //then
        assertEquals(expected, String.valueOf(actual));
        assertNotEquals(0, String.valueOf(actual));
    }

    @Test
    public void studentsOfGroup() {

        //given

        //when
        String expected = "[Valeria, Zozulia, Andreevna, International Economics, IE-15, " +
                "Anastasia, Chepurnova, Vitalievna, Economics, EC-15, " +
                "Platon, Voloshin, Nikolaevich, Economics, EC-15, " +
                "Katerina, Sherstyk, Ivanovna, International Economics, IE-15, " +
                "Anna, Chebotar, Nikolaevna, Management, Man-17, " +
                "Bohdan, Evtushenko, Andreevich, Management, Man-17 ]";
        List<String> actual = StudentList.studentsOfGroup(test);

        //then
        assertEquals(expected, String.valueOf(actual));
        assertNotEquals(0, String.valueOf(actual));
    }

    @Test
    public void countStudent() {

        //given
        String group = "Man-17";

        //when
        long expected = 2;
        long actual = StudentList.countStudent(test, group);

        //then
        assertEquals(expected, actual);
        assertNotEquals(0, actual);
    }
}
