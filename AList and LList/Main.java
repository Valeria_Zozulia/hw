package com.company;

public class Main {

    public static void main(String[] args) {

        AList aList = new AList(10);
        aList.print();
        aList.add(3);
        aList.print();
        aList.add(5, 5);
        aList.print();
        aList.removeByIndex(4);
        aList.print();
        aList.add(5);
        aList.print();
        aList.remove(5);
        aList.print();


    }
}
