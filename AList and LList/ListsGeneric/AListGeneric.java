package com.company;

import java.util.Arrays;

public class AListGeneric<E> implements IListGeneric {
    private static final int capacity = 10;
    private static final Object[] EMPTY_LIST = {};
    private Object[] array;
    private int size;

    public AListGeneric() {
        this.array = new Object[capacity];
    }

    public AListGeneric(int capacity) {
        if (capacity > 10) {
            this.array = new Object[capacity];
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    public AListGeneric(E[] array) {
        this.array = new Object[array.length];

        for (int i = 0; i < array.length; i++) {
            //this.array = array[i];
            this.size++;
        }
    }

    @Override
    public void clear() {
        this.array = EMPTY_LIST;
        this.size = 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public Object get(int index) {
        return null;
    }

    @Override
    public boolean add(Object number) {
        Object[] objects = Arrays.copyOf(array, (int) ((this.array.length + 1) * 100 / 20));
        objects[size] = number;
        this.array = objects;
        this.size++;
        return true;
    }

    @Override
    public boolean add(int index, Object number) {
        if (index >= 0 && index < this.size) {
            Object[] objects = new Object[(int) ((this.array.length + 1) * 100 / 20)];
            System.arraycopy(this.array, 0, objects, 0, index);
            objects[index] = number;
            System.arraycopy(this.array, index, objects, index + 1, this.size - index);

            this.array = objects;
            this.size++;
            return true;
        } else {
            throw new IndexOutOfBoundsException();
        }
    }

    @Override
    public Object remove(Object number) {
        return null;
    }

    @Override
    public Object removeByIndex(int index) {
        if (index >= 0 && index < this.size) {

        }
        return null;
    }

    @Override
    public boolean contains(Object number) {
        return false;
    }

    @Override
    public void print() {

    }

    @Override
    public Object[] toArray() {
        return new Object[0];
    }

    @Override
    public IListGeneric subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public boolean removeAll(Object[] arr) {
        for (int i = 0; i < this.size; i++) {
            for (Object val : arr) {
                this.remove(val);
            }
        }
        return true;
    }

    @Override
    public boolean retainAll(Object[] arr) {
        return false;
    }
}
