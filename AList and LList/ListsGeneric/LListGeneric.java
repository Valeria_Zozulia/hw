package com.company;


public class LListGeneric<E> implements IListGeneric<E> {
    private int size;
    private NodeGeneric<E> head;
    private NodeGeneric end;

    @Override
    public void clear() {
        head = null;
        end = null;
        size = 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public E get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        NodeGeneric<E> nodeGeneric = head;
        for (int i = 0; i < index; i++) {
            nodeGeneric = nodeGeneric.next;
        }
        return nodeGeneric.val;
    }

    @Override
    public boolean add(E number) {
        return false;
    }

    @Override
    public boolean add(int index, E number) {
        return true;

    }

    @Override
    public E remove(E number) {
        NodeGeneric nodeGeneric = head;
        NodeGeneric node = null;
        if (nodeGeneric != null && nodeGeneric.val.equals(number)) {
            head = nodeGeneric.next;
            return number;
        }
        while (nodeGeneric != null && !nodeGeneric.val.equals(number)) {
            node = nodeGeneric;
            nodeGeneric = nodeGeneric.next;
        }
        if (nodeGeneric == null) {
            return null;
        }
        node.next = nodeGeneric.next;
        size--;
        return number;
    }

    @Override
    public E removeByIndex(int index) {
        if (index < 0 || index < size - 1) {
            throw new IndexOutOfBoundsException();
        }
        if (index == 0) {
            NodeGeneric node = head;
            head = head.next;
            node.next = null;
        }
        return null;
    }

    @Override
    public boolean contains(E number) {
        for (int i = 0; i < size; i++) {
            if (get(i).equals(number)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void print() {

    }

    @Override
    public E[] toArray() {
        Object[] array = new Object[0];
        if (head == null) {

        }
        return null;
    }

    @Override
    public IListGeneric<E> subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public boolean removeAll(E[] arr) {
        return false;
    }

    @Override
    public boolean retainAll(E[] arr) {
        return false;
    }
}
