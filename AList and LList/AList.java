package com.company;

public class AList implements IList {

    private int[] temp;
    private int size;
    private static final int capacity = 10;
    private int index;


    public AList() {
        this.temp = new int[capacity];
    }

    public AList(int capacity) {
        this.temp = new int[capacity];
    }

    public AList(int[] arr) {
        this.temp = new int[(int) (arr.length + (arr.length * 0.2))];

        for (int i = 0; i < temp.length - 1; i++) {
            temp[i] = arr[i];
        }
    }

    @Override
    public void clear() {
        temp = new int[capacity];
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public int get(int index) {
        return temp[index];
    }

    @Override
    public boolean add(int number) {
        if (index == temp.length) {
            int[] array = new int[(int) (temp.length * 0.2) + temp.length];
        } else {
            temp[index] = number;
            index++;
            size++;
            return false;
        }
        return true;
    }

    @Override
    public boolean add(int index, int number) {
        if (index < 0 || index > temp.length) {
            return false;
        } else {
            temp[index] = number;
            return true;
        }
    }

    @Override
    public int remove(int number) {
        for (int i = 0; i < temp.length; i++) {
            if (temp[i] != number) {
                removeByIndex(i);
            } else {
                System.out.print("Mistake");
            }
        }
        return 0;
    }

    @Override
    public boolean removeByIndex(int index) {
        int j = 0;
        int[] array = new int[temp.length - 1];

        if (index < 0 || index > temp.length - 1) {
            return true;
        } else {
            for (int i = 0; i < array.length; i++) {
                if (i != index) {
                    array[j] = temp[i];
                }
            }
            temp = array;
            return true;
        }
    }

    @Override
    public boolean contains(int number) {
        for (int i = 0; i < size; i++) {
            if (temp[i] == number) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void print() {
        for (int i = 0; i < temp.length; i++) {
            if (i < temp.length - 1) {
                System.out.print("[" + temp[i] + "]");
            } else {
                new StringBuilder().append(temp[i]);
            }
        }
        System.out.println("\n");
    }

    @Override
    public int[] toArray() {
        int j = 0;
        int[] array = new int[0];

        for (int i = 0; i < temp.length; i++) {
            temp[i] = array[j];
        }
        return array;
    }

    @Override
    public IList subList(int fromIndex, int toIndex) {
        int[] temp1 = new int[toIndex - fromIndex];
        int j = 0;
        AList subList = new AList(temp1);

        for (int i = fromIndex; i < toIndex; i++) {
            temp1[j] = temp[i];
            j++;
        }
        return subList;
    }

    @Override
    public boolean removeAll(int[] arr) {
        for (int i = 0; i < temp.length; i++) {
            remove(temp[i]);
        }
        return false;
    }

    @Override
    public boolean retainAll(int[] arr) {
         return false;
    }
}
