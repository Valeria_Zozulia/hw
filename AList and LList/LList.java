package com.company;

public class LList implements IList {
    private int size = 0;
    private Node first;
    private Node last;

    @Override
    public void clear() {
        first = null;
        last = null;
        size = 0;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public int get(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        Node node = first;

        for (int i = 0; i < index; i++) {
            node = node.next;
        }
        return node.val;
    }

    @Override
    public boolean add(int number) {
        Node newNode = new Node(number);

        if (first == null) {
            newNode.next = null;
            first = newNode;
            last = newNode;
        } else {
            last.next = newNode;
            last = newNode;
        }
        size++;
        return true;
    }

    @Override
    public boolean add(int index, int number) {
        Node newNode = new Node(number);

        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException();
        }
        if (index == 0) {
            add(number);
        }
        if (index == size) {
            last.next = newNode;
            last = newNode;
        } else {
            Node nodePrevious = first;

            for (int i = 0; i <= index; i++) {
                if (i != index) {
                    nodePrevious = moveNext(nodePrevious);
                } else {
                    Node node = new Node(number);
                    node.next = nodePrevious.next;
                    nodePrevious.next = node;

                    size++;
                    return true;
                }
            }
        }
        return false;
    }

    @Override
    public int remove(int number) {

        return 0;
    }

    @Override
    public boolean removeByIndex(int index) {
        if (index < 0 || index > size - 1) {
            throw new IndexOutOfBoundsException();
        }
        Node node = first;
        for (int i = 0; i < index; i++) {
            if (i == index) {

            }
        }
        return true;
    }

    @Override
    public boolean contains(int number) {
        for (int i = 0; i < size; i++) {
            Node node = first;

            if (node.val == number) {
                return true;
            }
        }
        return false;
    }

    @Override
    public void print() {
        Node node = first;

        for (int i = 0; i <= size; i++) {
            if ()
        }
    }

    @Override
    public int[] toArray() {
        return new int[0];
    }

    @Override
    public IList subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public boolean removeAll(int[] arr) {
        return false;
    }

    @Override
    public boolean retainAll(int[] arr) {
        return false;
    }

    public Node moveNext(Node node) {
        if (node == null) {
            return node;
        }
        if (node.next == null) {
            System.out.println("It's node the last one.");
            return node;
        }
        return node.next;
    }
}
