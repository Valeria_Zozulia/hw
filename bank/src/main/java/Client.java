import java.util.Random;
import java.util.concurrent.CountDownLatch;

public class Client implements Runnable {

    private String name;
    private boolean ready = false;
    private boolean takeCredit = false;
    private boolean returnCredit = false;
    private boolean putDeposit = false;
    private boolean takeDeposit = false;

    public Client() {
    }

    public Client(String name) {
        this.name = name;
    }

    public boolean takeCredit() {
        return takeCredit();
    }

    @Override
    public void run() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    public boolean isTake() {
        return takeCredit;
    }

    public void setTake(boolean take) {
        this.takeCredit = take;
    }

    public boolean isTakeCredit() {
        return takeCredit;
    }

    public void setTakeCredit(boolean takeCredit) {
        this.takeCredit = takeCredit;
    }

    public boolean isReturnCredit() {
        return returnCredit;
    }

    public void setReturnCredit(boolean returnCredit) {
        this.returnCredit = returnCredit;
    }

    public boolean isPutDeposit() {
        return putDeposit;
    }

    public void setPutDeposit(boolean putDeposit) {
        this.putDeposit = putDeposit;
    }

    public boolean isTakeDeposit() {
        return takeDeposit;
    }

    public void setTakeDeposit(boolean takeDeposit) {
        this.takeDeposit = takeDeposit;
    }
}


