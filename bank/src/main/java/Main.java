import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        Client client = new Client();
        Thread thread = new Thread(client);
        thread.start();
    }
}
