import java.util.List;
import java.util.concurrent.*;

public class Cashier implements Runnable {
    private CountDownLatch countDownLatch;

    public Cashier(CountDownLatch countDownLatch) {
        this.countDownLatch = countDownLatch;
    }

    public static void cashier() {
        //ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        //ScheduledFuture<?> future = executor.scheduleAtFixedRate(() -> {

        List<BankAccount> bankAccountList = BankAccount.getBankAccountList();
        //BankAccount.setCredit();
        //BankAccount.setDeposit();

        for (BankAccount bankAccount : bankAccountList) {

            //if () {
            if (bankAccount.returnCredit(bankAccount)) {
                BankAccount.takeCredit(bankAccount);
                BankAccount.removeAccount(bankAccount);
                System.out.println("12");
            } else {
                BankAccount.takeDeposit(bankAccount);
                BankAccount.removeAccount(bankAccount);
                System.out.println("13");
            }
        }
    }
    //BankAccount.setIsAvailable1(true);

    //BankAccount.setIsAvailable2(true);



    @Override
    public void run() {

        try {
            Thread.sleep(150);
            cashier();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
