public class Constants {
    public static final int BALANCE = 50000;
    public static final int MAX_BALANCE = 1000000;
    public static final int COUNT_TRANSACTIONS = 10000;

    public static final int MIN_CREDIT = 500;
    public static final int MAX_CREDIT = 20000;

    public static final int MIN_DEPOSIT = 1000;
    public static final int MAX_DEPOSIT = 20000;

    public static final int MIN_PERIOD_DEPOSIT = 120;
    public static final int MAX_PERIOD_DEPOSIT = 360;

    public static final int MIN_PERCENT_RATE_CREDIT = 9;
    public static final int MAX_PERCENT_RATE_CREDIT = 18;

    public static final int MIN_PERCENT_RATE_DEPOSIT = 6;
    public static final int MAX_PERCENT_RATE_DEPOSIT = 9;
}
