import java.util.concurrent.CountDownLatch;

public class Checker implements Runnable {
    private void check() {
        CountDownLatch countDownLatch = new CountDownLatch(1);

        if (BankAccount.getCreditAmount() >= BankAccount.getDepositAmount() = 0) {
            System.out.println("Not enough for credit. Please wait.");
            BankAccount.setCreditAmount(BankAccount.getCreditAmount());
        } else {
            BankAccount.setDepositAmount(BankAccount.getDepositAmount());
        }

        if (BankAccount.getDepositAmount() <= BankAccount.getCreditAmount()) {
            System.out.println("Not enough for deposit. Please wait.");
            BankAccount.setDepositAmount(BankAccount.getDepositAmount());
        } else {
            BankAccount.setCreditAmount(BankAccount.getCreditAmount());
        }

    }

    @Override
    public void run() {
        check();
    }
}
