import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

public class BankAccount implements Runnable {
    private static int balance = 50000;
    private static int credit;
    private static int deposit;
    private static int periodCredit;
    private static int periodDeposit;
    private static int percentRateCredit;
    private static int percentRateDeposit;
    private static int creditAmount;
    private static int depositAmount;

    BankAccount bankAccount = new BankAccount();
    ReentrantLock locker = new ReentrantLock();
    Random random = new Random();

    public static int getCreditAmount() {
        return creditAmount;
    }

    public static void setCreditAmount(int creditAmount) {
        BankAccount.creditAmount = creditAmount;
    }

    public static int getDepositAmount() {
        return depositAmount;
    }

    public static void setDepositAmount(int depositAmount) {
        BankAccount.depositAmount = depositAmount;
    }

    //get
    public int getBalance() {
        return balance;
    }

    public int getCredit() {
        return credit;
    }

    public int getDeposit() {
        return deposit;
    }

    public int getPeriodCredit() {
        return periodCredit;
    }

    public int getPeriodDeposit() {
        return periodDeposit;
    }

    //set
    public void setBalance(int balance) {

        this.balance = balance;
    }

    public void setCredit(int credit) {
        this.credit = credit;
    }

    public void setDeposit(int deposit) {
        this.deposit = deposit;
    }

    public void setPeriodCredit(int periodCredit) {
        this.periodCredit = periodCredit;
    }

    public void setPeriodDeposit(int periodDeposit) {
        this.periodDeposit = periodDeposit;
    }

    public boolean withdraw(int amount) {
        if (this.deposit >= balance) {
            this.deposit -= amount;
            return true;
        }
        return false;
    }

    public static boolean takeCredit(BankAccount bankAccount) {
        credit = (int) (Math.random() * (Constants.MAX_CREDIT + 1)) + Constants.MIN_CREDIT;
        bankAccount.setBalance(bankAccount.getBalance() - credit);
        balance = balance - credit;
        System.out.println(("Your credit: " + credit) + ("Balance: " + bankAccount.getBalance()));
        return false;
    }

    public static boolean returnCredit(BankAccount bankAccount) {
        percentRateCredit = (int) (Math.random() * (Constants.MAX_PERCENT_RATE_CREDIT + 1)) + Constants.MIN_PERCENT_RATE_CREDIT;
        balance = balance + (credit + (credit * percentRateCredit));
        System.out.println("Return credit, new balance is: " + bankAccount.getBalance());
        return false;
    }


    public void putDeposit(BankAccount bankAccount) {
        deposit = (int) (Math.random() * (Constants.MAX_DEPOSIT + 1)) + Constants.MIN_DEPOSIT;
        bankAccount.setBalance(bankAccount.getBalance() + deposit);
        balance = balance + deposit;
        System.out.println(("Deposit: " + deposit) + ("Balance: " + bankAccount.getBalance()));
    }

    public static boolean takeDeposit(BankAccount bankAccount) {
        percentRateDeposit = (int) (Math.random() * (Constants.MAX_PERCENT_RATE_DEPOSIT + 1) + Constants.MIN_PERCENT_RATE_DEPOSIT);
        balance = balance - (deposit - (deposit * percentRateDeposit));
        System.out.println("Take deposit: " + deposit + "New balance is: " + bankAccount.getBalance());
        return false;
    }

    public void run() {
//        int makeChoice = random.nextInt(2);
//
//        if (makeChoice == 0) {
//            takeCredit();
//        } else if (makeChoice == 1) {
//            returnCredit();
//        } else if (makeChoice == 2) {
//            putDeposit();
//        } else if (makeChoice == 3) {
//            takeDeposit();
//        }
    }

    private void makeWithdrawal(int amount) {
        if (getBalance() >= amount) {
            System.out.println(Thread.currentThread().getName());

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            withdraw(amount);
            System.out.println(Thread.currentThread().getName() + "The balance is " + getBalance());
        } else {
            System.out.println("Not enough in account for " + Thread.currentThread().getName() + getBalance());
        }
    }

    public static List<BankAccount> bankAccountList = new LinkedList<>();


    public static synchronized List<BankAccount> getBankAccountList() {
        return bankAccountList;
    }

    public static synchronized void setBankAccountList(List<BankAccount> bankAccountList) {
        BankAccount.bankAccountList = bankAccountList;
    }

    public static synchronized void addBankAccountToList(BankAccount bankAccount) {
        bankAccountList.add(bankAccount);

    }

    public static synchronized void removeAccount(BankAccount bankAccount) {
        System.out.println("10000!");
        bankAccountList.remove(bankAccount);
    }

}
