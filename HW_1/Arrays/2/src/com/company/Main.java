package com.company;

public class Main {

    public static void main(String[] args) {
        int[] num = new int[]{1, 2, 3, 4, 5};
        int max = num[0];
        for (int i = 0; i < num.length; i++) {
            if (max < num[i]) {
                max = num[i];
            }
        }
        System.out.println("max: " + max);
    }
}