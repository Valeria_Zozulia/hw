package com.company;

public class Main {

    public static void main(String[] args) {
        int[] num = new int[10];
        for (int i = 0; i < num.length; i++) {
            num[i] = (int) (1 + Math.random() * 50);
            System.out.print("[" + num[i] + "]");
        }
        int sum = 0;
        for(int i = 0; i < num.length; i++){
            if(num[i] % 2 != 0){
                sum = sum + 1;
            }
        }
        System.out.println("\n" + "Sum: " + sum);
    }
}
