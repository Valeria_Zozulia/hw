package com.company;

public class Main {

    public static void main(String[] args) {
        int[] num = new int[]{3, 4, 1, 5, 2};
        int sum = 0;
        for (int i = 0; i < num.length; i++) {
            if (i % 2 != 0) {
               sum = sum + num[i];
            }
        }
        System.out.println("Sum: " + sum);
    }
}
