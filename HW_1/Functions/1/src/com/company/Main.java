package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите номер дня недели: ");
        int a = in.nextInt();
        Function(a);
    }

    public static void Function(int a) {
        String[] week = {"Monday", "Tuesday", "Wednesday", "Thursday",
                "Friday", "Saturday", "Sunday"};
        if (a <= week.length && a > 0) {
            System.out.print(week[a - 1]);
            return ;
        } else{
            System.out.print("Not found!");
        }
    }
}