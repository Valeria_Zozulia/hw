package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Enter x: ");
        int x = in.nextInt();
        System.out.print("Enter y: ");
        int y = in.nextInt();
        Function(x, y);
    }

    public static void Function(int x, int y) {
        int result = 0;
            if (x >= 0 && y >= 0) {
                result = x + y;
            } else if (x <= 0 && y >= 0) {
                result = Math.abs(x) + y;
            } else if (x >= 0 && y <= 0) {
                result = x + Math.abs(y);
            } else if (x <= 0 && y <= 0) {
                result = Math.abs(x + y);
            }
        System.out.print("Your result: " + result);
    }
}
