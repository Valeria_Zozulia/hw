package com.company;
import java.util.Scanner;
public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите число: ");
        int a = in.nextInt();
        int result = 0;
        int c = a;
        for (int i = 1; i <= a; i = i * 10) {
            result += c % 10;
            c = c / 10;
        }
        System.out.println(result);
    }
}

