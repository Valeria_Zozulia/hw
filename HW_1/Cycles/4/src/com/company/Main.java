package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите число:");
        int a = in.nextInt();
        int b = 1;
        for (int i = 1; i <= a; i++) {
            b = b * i;
        }
        System.out.println(b);
    }
}
