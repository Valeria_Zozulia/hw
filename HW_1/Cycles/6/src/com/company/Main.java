package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите число: ");
        int a = in.nextInt();
        String result = "";
        int c = a;
        for (int i = 1; i <= a; i = i * 10) {
            int num = (c % 10);
            result = result + Integer.toString(num);
            c = c / 10;
        }
        System.out.println("result: " + result);
    }
}
