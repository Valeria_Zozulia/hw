package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите кол-во баллов: ");
        int a = in.nextInt();
        if (a > 0 && a <= 19){
            System.out.println("Ваша оценка F");
        } else if (a >= 20 && a <= 39){
            System.out.println("Ваша оценка E");
        } else if (a >= 40 && a <= 59){
            System.out.println("Ваша оценка D");
        } else if (a >= 60 && a <= 74){
            System.out.println("Ваша оценка С");
        } else if (a >= 75 && a <= 89){
            System.out.println("Ваша оценка B");
        } else if (a >= 90 && a <= 100){
            System.out.println("Ваша оценка A");
        } else if (a < 0){
            System.out.println("Такого не бывает");
        }
    }
}
