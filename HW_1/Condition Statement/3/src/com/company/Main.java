package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите число а: ");
        int a = in.nextInt();
        System.out.println("Введите число b: ");
        int b = in.nextInt();
        System.out.println("Введите число c: ");
        int c = in.nextInt();
        if (a < 0 && b < 0 && c < 0
                || a < 0 && b < 0 && c > 0
                || a > 0 && b < 0 && c < 0
                || a < 0 && b > 0 && c < 0){
            System.out.println("Невозможно посчитать сумму!");
        } else if (a >= 0 && b >= 0 && c >= 0) {
            System.out.println(a + b + c);
        } else if (a >= 0 && b >= 0 && c <= 0) {
            System.out.println(a + b);
        } else if (a >= 0 && b <= 0 && c >= 0) {
            System.out.println(a + c);
        } else if (a <= 0 && b >= 0 && c >= 0) {
            System.out.println(b + c);
        } else {
            System.out.println();
        }
    }
}