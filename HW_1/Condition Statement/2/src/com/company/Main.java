package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите координату х:");
        int x = in.nextInt();
        System.out.println("Введите ккординату у:");
        int y = in.nextInt();
        if (x == 0 || y == 0){
            System.out.println("Невозможно определить координатную плоскость");
        }
        if (x > 0 && y > 0) {
            System.out.println("Точка принадлежит первой координатной плоскости");
        } else if (x < 0 && y > 0) {
            System.out.println("Точка принадлежит второй координатной плоскости");
        } else if (x < 0 && y < 0){
            System.out.println("Точка принадлежит третьей координатной плоскости");
        } else if (x > 0 && y < 0){
            System.out.println("Точка принадлежит четвертой координатной плоскости");
        }else {
            System.out.println();
        }

    }
}