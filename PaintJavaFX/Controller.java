package sample;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.image.WritableImage;
import javafx.stage.FileChooser;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;


public class Controller {

    @FXML
    private Canvas canvas;

    @FXML
    private ColorPicker colorPicker;

    @FXML
    private Slider slider;

    @FXML
    private Label label;

    @FXML
    private CheckBox eraser;

    public void initialize() {
        GraphicsContext graphicsContext = canvas.getGraphicsContext2D();

        slider.valueProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue) {
                label.setText(String.format("%.0f", newValue));
                graphicsContext.setLineWidth(slider.getValue());
            }
        });

        colorPicker.setOnAction(e -> {
            graphicsContext.setStroke(colorPicker.getValue());

        });

        canvas.setOnMousePressed(e -> {
            graphicsContext.beginPath();
            graphicsContext.lineTo(e.getX(), e.getY());
            graphicsContext.stroke();
        });

        canvas.setOnMouseDragged(e -> {
            graphicsContext.lineTo(e.getX(), e.getY());
            graphicsContext.setStroke(colorPicker.getValue());
            double size = slider.getValue();
            double x = e.getX() - size / 2;
            double y = e.getY() - size / 2;

            if (eraser.isSelected()) {
                graphicsContext.clearRect(x, y, size, size);
            } else {
                graphicsContext.setFill(colorPicker.getValue());
                graphicsContext.strokeOval(x, y, size, size);
                graphicsContext.fillRect(x, y, size, size);

            }
        });
    }

    public void onSave() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Save");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("PNG files (*.png)", "*.png"));

        fileChooser.setInitialFileName("paint file");
        fileChooser.setInitialDirectory(new File("C:\\Users\\valer\\OneDrive\\Изображения\\Saved Pictures"));
        File file = fileChooser.showSaveDialog(null);

        if (file != null) {
            try {
                WritableImage writableImage = canvas.snapshot(null, null);
                ImageIO.write(SwingFXUtils.fromFXImage(writableImage, null), "png", file);
            } catch (IOException e) {
                System.out.println("Failed to save image" + e);
            }
        }
    }


    public void onExit() {
        Platform.exit();
    }
}

