package sample;

import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;

public class Controller {

    @FXML
    TextField writeValueFrom;

    @FXML
    TextField writeValueTo;

    @FXML
    ComboBox<Category> category = new ComboBox<>();

    @FXML
    ComboBox<String> convertFrom = new ComboBox<>();

    @FXML
    ComboBox<String> convertTo = new ComboBox<>();

    @FXML
    public void initialize() {
        category.getItems().setAll(Category.values());
        category.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> convertFrom.setValue(Category.getUnit0(newValue)));
        category.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> convertTo.getItems().setAll(Category.getAllUnits(newValue)));
        category.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> convertTo.setValue(Category.getUnit1(newValue)));
    }

    @FXML
    private void changeCategoryAction() {
        writeValueFrom.setText("");
        writeValueTo.setText("");
    }

    @FXML
    private void convert() {
        Boolean firstNumToSecondNum = false;
        String categoryOfMetrics = parseComBoBox(category);
        String firstMetric = parseComBoBox(convertFrom);
        String secondMetric = parseComBoBox(convertTo);

        double firstNum = 0;
        double secondNum = 0;

        if (writeValueFrom.isFocused()) {
            firstNum = parseTextField(writeValueFrom);
            firstNumToSecondNum = true;
        } else {
            secondNum = parseTextField(writeValueTo);
        }

        switch (categoryOfMetrics) {
            case "LENGTH":
                if (firstNumToSecondNum) {
                    writeValueTo.setText(Function.lengthConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                } else {
                    writeValueFrom.setText(Function.lengthConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                }
                break;
            case "TEMPERATURE":
                if (firstNumToSecondNum) {
                    writeValueTo.setText(Function.temperatureConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                } else {
                    writeValueFrom.setText(Function.temperatureConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                }
                break;
            case "WEIGHT":
                if (firstNumToSecondNum) {
                    writeValueTo.setText(Function.weightConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                } else {
                    writeValueFrom.setText(Function.weightConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                }
                break;
            case "TIME":
                if (firstNumToSecondNum) {
                    writeValueTo.setText(Function.convertTime(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                } else {
                    writeValueFrom.setText(Function.convertTime(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                }
                break;
            case "VOLUME":
                if (firstNumToSecondNum) {
                    writeValueTo.setText(Function.volumeConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                } else {
                    writeValueFrom.setText(Function.volumeConvert(firstMetric, secondMetric, firstNum, secondNum, firstNumToSecondNum));
                }
                break;
        }
    }

    @FXML
    public Double parseTextField(TextField textField) {

        try {
            Double number = Double.parseDouble(textField.getText());
            return number;
        } catch (Exception e) {
            return 0.0;
        }
    }

    @FXML
    public String parseComBoBox(ComboBox comboBox) {

        String unitValue = comboBox.getValue().toString();
        return unitValue;
    }

}


