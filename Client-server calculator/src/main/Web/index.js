class Calculator {
    constructor(firstNumberTextElement, secondNumberTextElement) {
        this.firstNumberTextElement = firstNumberTextElement;
        this.secondNumberTextElement = secondNumberTextElement;
    }

    clear() {
        this.secondNumber = '';
        this.firstNumber = '';
        this.operation = undefined;
    }

    delete() {
        this.firstNumber = this.seconNumber.toString().slice(0, -1);
    }

    appendNumber(number) {
        if (number === '.' && this.secondNumber.includes('.')) return
        this.secondNumber = this.secondNumber ? this.secondNumber + number : number;

    }

    chooseOperation(operation) {
        if (this.secondNumber === '') return
        if (this.firstNumber !== '') {
            //this.compute()
        }
        this.operation = operation;
        this.firstNumber = this.secondNumber;
        this.secondNumber = ' ';
    }

    compute() {
        let result;
        const first = Number(this.firstNumber);
        const second = Number(this.secondNumber);
        if (!isNaN(first) || !isNaN(second)) {

            switch (this.operation) {
                case "+":
                    result = first + second;
                    break;
                case '*':
                    result = first * second;
                    break;
                case '-':
                    result = first - second;
                    break;
                case '/':
                    result = first / second;
                    break;
                default:
                    return "Error";
            }
            this.firstNumber = '';
            this.secondNumber = result;
            this.operation = undefined;
        }
    };

    getDisplayNumber(number) {
        const stringNumber = number.toString();
        const integerDigits = parseFloat(stringNumber.split('.')[0]);
        const decimalDigits = stringNumber.split('.')[1];
        let integerDisplay;

        if (isNaN(integerDigits)) {
            integerDisplay = '';
        } else {
            integerDisplay = integerDigits.toLocaleString('en', {
                maximumFractionDigits: 0,
            })
        }

        if (decimalDigits != null) {
            return `${integerDisplay}.${decimalDigits}`;
        } else {
            return integerDisplay;
        }
    }

    updateDisplay() {
        this.secondNumberTextElement.innerText = this.getDisplayNumber(this.secondNumber);
        if (this.operation != null) {
            this.firstNumberTextElement.innerText =
                `${this.getDisplayNumber(this.firstNumber)} ${this.operation}`;
        } else {
            this.firstNumberTextElement.innerText = '';
        }
    }
}

const numberButtons = document.querySelectorAll('[data-number]');
const operationButtons = document.querySelectorAll('[data-operation]');
const equalsButton = document.querySelector('[data-equals]');
const deleteButton = document.querySelector('[data-delete]');
const allClearButton = document.querySelector('[data-all-clear]');
const previousOperandTextElement = document.querySelector('[data-previous-operand]');
const currentOperandTextElement = document.querySelector('[data-current-operand]');

const calculator = new Calculator(previousOperandTextElement, currentOperandTextElement)

numberButtons.forEach(button => {
    button.addEventListener('click', () => {
        calculator.appendNumber(button.innerText);
        calculator.updateDisplay();
    });
});

operationButtons.forEach(button => {
    button.addEventListener('click', () => {
        calculator.chooseOperation(button.innerText);
        calculator.updateDisplay();
    });
});

equalsButton.addEventListener('click', button => {
    calculator.compute();
    calculator.updateDisplay();
})


allClearButton.addEventListener('click', button => {
    calculator.clear();
    calculator.updateDisplay();
});

deleteButton.addEventListener('click', button => {
    calculator.delete();
    calculator.updateDisplay();
});



