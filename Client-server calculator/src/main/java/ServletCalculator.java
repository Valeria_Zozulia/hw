import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class ServletCalculator extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {

        String previousOperand = request.getParameter("previousOperand");
        String currentOperand = request.getParameter("currentOperand");
        String operation = request.getParameter("operation");

        double previousNumber = 0;
        double currentNumber = 0;
        boolean noError = true;

        try {
            previousNumber = Double.parseDouble(previousOperand);
            currentNumber = Double.parseDouble(currentOperand);
        } catch (Exception e) {
            noError = false;
        }

        if (noError) {
            double result = 0;

            try {
                if (operation.equals("+")) {
                    result = sum(previousNumber, currentNumber);
                } else if (operation.equals("-")) {
                    result = subtract(previousNumber, currentNumber);
                } else if (operation.equals("*")) {
                    result = multiply(previousNumber, currentNumber);
                } else if (operation.equals("/")) {
                    result = divide(previousNumber, currentNumber);
                } else {
                    noError = false;
                }
            } catch (Exception ex) {
                noError = false;
            }

            if (noError) {
                showResult(response, result);
            }
        }
        showError(response);
    }

    private double sum(double firstNumber, double secondNumber) {
        return firstNumber + secondNumber;
    }

    private double subtract(double firstNumber, double secondNumber) {
        return firstNumber - secondNumber;
    }

    private double multiply(double firstNumber, double secondNumber) {
        return firstNumber * secondNumber;
    }

    private double divide(double firstNumber, double secondNumber) {
        return firstNumber / secondNumber;
    }

    private void showResult(HttpServletResponse response, double result) throws IOException {
        String reply = "{\"error\":0,\"result\":" + Double.toString(result) + "}";
        response.getOutputStream().write( reply.getBytes("UTF-8") );
        response.setContentType("application/json; charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setStatus( HttpServletResponse.SC_OK );
    }

    private void showError(HttpServletResponse response) throws IOException {
        String reply = "{\"error\":1}";
        response.getOutputStream().write( reply.getBytes("UTF-8") );
        response.setContentType("application/json; charset=UTF-8");
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setStatus( HttpServletResponse.SC_OK );
    }
}
