package sample;

import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class Controller {

    @Override
    public void start(Stage primaryStage) {

        Pane canvas = new Pane();
        Scene scene = new Scene(canvas, 1000, 1000, Color.BISQUE);
        Circle ball = new Circle(50, Color.CRIMSON);
        ball.relocate(5, 5);

        canvas.getChildren().add(ball);

        primaryStage.initStyle(StageStyle.);
    }
}
