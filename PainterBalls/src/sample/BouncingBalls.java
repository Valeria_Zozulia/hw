package sample;

import javafx.scene.control.ColorPicker;
import javafx.scene.paint.Color;

public class BouncingBalls extends DrawingApp {
    BouncingBalls[] balls = new BouncingBalls[1000];

    public static void main(String[] args) {
        launch();
    }

    @Override
    void setUp() {
        width = 1000;
        height = 1000;

        for(int i = 0; i < balls.length; i++) {
            balls[i] = new BouncingBalls();
        }
    }

    @Override
    void draw() {
        graphicsContext.setFill(Color.CYAN);
        graphicsContext.fillRect(0, 0, width, height);

        for(BouncingBalls ball : balls) {
            ball.update();
            ball.show();
        }
    }

    class Ball {
        int x, y, r, speedX, speedY;
        Color color;

        public Ball(int x, int y) {
            this.x = x;
            this.y = y;
            this.color = Color.rgb(random.nextInt(255), random.nextInt(255), random.nextInt(255));
            r = random.nextInt(100);
            speedX = 40 - random.nextInt(40) - 20;
            speedY = random.nextInt(40) - 20;
        }

        void update() {
            this.x += speedX;
            this.y += speedY;

            if(x < 0 || x > width) {
                speedX *= -1;
            }
            if(y < 0 || y > height) {
                speedY *= -1;
            }
        }

        void show() {
            graphicsContext.setFill(color);
            graphicsContext.fillOval(x, y, r, r);
        }
    }
}
