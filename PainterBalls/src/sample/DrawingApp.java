package sample;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import javafx.util.Duration;

import java.util.Random;

public abstract class DrawingApp extends Application {
    public int frames = 10;

    public static int width = 1000;
    public static int height = 1000;

    public static Random random = new Random();

    Canvas canvas = new Canvas();
    GraphicsContext graphicsContext = canvas.getGraphicsContext2D();

    @Override
    public void start(Stage primaryStage) throws Exception {
        setUp();
        canvas.setHeight(height);
        canvas.setWidth(width);

        canvas.setOnMouseClicked(this::mouseClicked);
        canvas.setOnMouseDragged(this::mouseDragged);
        canvas.setOnMouseMoved(this::mouseMoved);
        canvas.setOnMouseEntered(this::mouseEntered);
        canvas.setOnMouseExited(this::mouseExited);
        canvas.setOnKeyPressed(this::keyPressed);
        canvas.setOnKeyTyped(this::keyTyped);
        canvas.setOnKeyReleased(this::keyReleased);

        canvas.requestFocus();
        KeyFrame keyFrame = new KeyFrame(Duration.millis(1000 / frames), e -> draw());
        Timeline timeline = new Timeline(keyFrame);
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

    void setUp() {
    }

    void draw() {
    }

    public void mouseClicked(MouseEvent event) {
        mouseClicked(event);
    }

    public void mouseDragged(MouseEvent event) {
        mouseDragged(event);
    }

    public void mouseMoved(MouseEvent event) {
        mouseMoved(event);
    }

    public void mouseEntered(MouseEvent event) {
        mouseEntered(event);
    }

    public void mouseExited(MouseEvent event) {
        mouseExited(event);
    }

    public void keyPressed(KeyEvent event) {
        keyPressed(event);
    }

    public void keyTyped(KeyEvent event) {
        keyTyped(event);
    }

    public void keyReleased(KeyEvent event) {
        keyReleased(event);
    }

}
