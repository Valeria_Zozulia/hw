package com.company;

public class Main {

    public static void main(String[] args) {
        System.out.println(bigLettersEnglishAlphabet());
        System.out.println(reverseSmallLettersEnglishAlphabet());
        System.out.println(smallLettersRusAlphabet());
        System.out.println(numbers());
        System.out.println(ascii());
    }

    public static String bigLettersEnglishAlphabet() {
        String str1 = "";

        for (char i = 'A'; i <= 'Z'; i++) {
            str1 += i;
        }
        return str1;
    }

    public static String reverseSmallLettersEnglishAlphabet() {
        String str2 = "";

        for (char i = 'a'; i <= 'z'; i++) {
            str2 += i;
        }
        return new StringBuilder(str2).reverse().toString();
    }

    public static String smallLettersRusAlphabet() {
        String str3 = "";

        for (char i = 'а'; i <= 'я'; i++) {
            str3 += i;
        }
        return str3;
    }

    public static String numbers() {
        String str4 = "";

        for (char i = '0'; i <= '9'; i++) {
            str4 += i;
        }
        return str4;
    }

    public static String ascii() {
        String srt5 = "";

        for (char i = ' '; i < 274; i++) {
            srt5 += i;
        }
        return srt5;
    }
}


