package com.company;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Введите скорость первого автомобиля: ");
        double v1 = in.nextDouble();

        System.out.print("Введите скорость второго автомобиля: ");
        double v2 = in.nextDouble();

        System.out.print("Введите растояние между автомобилями: ");
        double s = in.nextDouble();

        System.out.print("Введите время: ");
        double t = in.nextDouble();

        System.out.println("Проехал первый автомобиль: " + FirstCarDistance(v1, t));
        System.out.println("Проехал второй автомобиль: " + SecondCarDistance(v2, t));
        System.out.println("Расстояние между автомобилями: " + distanceBetweenCars(v1, v2, t, s));
    }

    public static double FirstCarDistance(double v1, double t) {
        return v1 * t;
    }

    public static double SecondCarDistance(double v2, double t) {
        return v2 * t;
    }

    public static double distanceBetweenCars(double v1, double v2, double t, double s) {
        return FirstCarDistance(v1, t) + SecondCarDistance(v2, t) + s;
    }
}


